# Spring Boot Reactive Webflux Server-Sent Events
This is a sample application that shows how to use Server-Sent Events using:

* Spring Boot 2
* Spring Webflux

Please see the following pages for more details
* Spring Web Reactive
http://docs.spring.io/spring-framework/docs/5.0.0.M1/spring-framework-reference/html/web-reactive.html

#### Running
Run this using using the gradle wrapper included

`./gradlew clean bootRun`

This will start the application on port 8080.

#### cURL Commands
You can try the following API once the server is running.

GET /stock/transaction

`curl -v http://localhost:8080/stock/transaction`